#!/bin/bash
echo "Removing old container"
#sudo docker ps -a | awk '{ print $1,$2 }' | grep cicd | awk '{print $1 }' | xargs -I {} docker rm {}
sudo docker stop cicd
sudo docker rm cicd
echo "Building container"
sudo docker build -t cicd .
echo "Running container"
sudo docker run --name cicd -dit -p 4000:4000 -v /mnt/glusterfs/:/app/uploads cicd
