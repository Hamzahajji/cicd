FROM node:8.16.0-alpine

WORKDIR /app

RUN mkdir uploads

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 4000

CMD ["node", "index.js"]
