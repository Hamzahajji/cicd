const express = require('express');
const app = express();
const fileUpload = require('express-fileupload');

app.use('/uploads', express.static(__dirname + "/uploads"));

app.use(fileUpload());

// Homepage
app.get('/', function (req, res) {
  res.send('CI/CD working!');
});

// Upload page
app.get('/upload', function (req, res) {
  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.write("<form ref='uploadForm' id='uploadForm' action='http://localhost:4000/upload' method='post' encType='multipart/form-data'>");
  res.write("<input type='file' name='sampleFile' />");
  res.write("<input type='submit' value='Upload!' />");
  res.write("</form>");

  // Upload file
  app.post('/upload', function (req, res) {
    let sampleFile;
    let uploadPath;
    let mimeType;
    if (Object.keys(req.files).length == 0) {
      res.status(400).send('No files were uploaded.');
      return;
    }
    mimeType = req.files.sampleFile.mimetype;
    // Check files extension (Only allow png/jpg/jpeg) 
    if (mimeType === 'image/png' || mimeType === 'image/jpg' || mimeType === 'image/jpeg') {
      console.log('req.files >>>', req.files); // eslint-disable-line

      sampleFile = req.files.sampleFile;

      uploadPath = __dirname + '/uploads/' + sampleFile.name;

      sampleFile.mv(uploadPath, function (err) {
        if (err) {
          return res.status(500).send(err);
        }

        res.send('File uploaded to ' + uploadPath);
      });

    } else {
      res.status(400).send('File type not allowed.');
      return;
    }
  });
});
app.listen(4000);
